package com.example.scott_000.dutraffic;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class LogInPage extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in_page);

        Button buttonSignIn = (Button) findViewById(R.id.buttonSignIn);
        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(v.getContext(), MainPage.class);
                startActivityForResult(intent, 0);
            }
        });

        //final ToMain goTomain =;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_log_in_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static class MainPage extends ActionBarActivity {

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main_page);

            Button SearchButton = (Button) findViewById(R.id.SearchButton);
            SearchButton.setOnClickListener(new View.OnClickListener()

            {
                @Override
                public void onClick(View v) {


                    Intent intent = new Intent(v.getContext(), DUTRAFFICMAP.class);
                    startActivityForResult(intent, 0);
                }
            });
        }


        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_main_page, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                return true;
            }

            return super.onOptionsItemSelected(item);
        }
    }


    public static class DUTRAFFICMAP extends FragmentActivity {

        private GoogleMap mMap; // Might be null if Google Play services APK is not available.


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_dutrafficmap);
            setUpMapIfNeeded();

        }

        @Override
        protected void onResume() {
            super.onResume();
            setUpMapIfNeeded();
        }

        /**
         * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
         * installed) and the map has not already been instantiated.. This will ensure that we only ever
         * call {@link #setUpMap()} once when {@link #mMap} is not null.
         * <p/>
         * If it isn't installed {@link SupportMapFragment} (and
         * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
         * install/update the Google Play services APK on their device.
         * <p/>
         * A user can return to this FragmentActivity after following the prompt and correctly
         * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
         * have been completely destroyed during this process (it is likely that it would only be
         * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
         * method in {@link #onResume()} to guarantee that it will be called.
         */
        private void setUpMapIfNeeded() {
            // Do a null check to confirm that we have not already instantiated the map.
            if (mMap == null) {
                // Try to obtain the map from the SupportMapFragment.
                mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                        .getMap();
                // Check if we were successful in obtaining the map.
                if (mMap != null) {
                    setUpMap();
                }
            }
        }

        /**
         * This is where we can add markers or lines, add listeners or move the camera. In this case, we
         * just add a marker near Africa.
         * <p/>
         * This should only be called once and when we are sure that {@link #mMap} is not null.
         */
        private void setUpMap() {
            mMap.addMarker(new MarkerOptions().position(new LatLng(39.95388, -75.187014)).title("Main Building"));
            mMap.addMarker(new MarkerOptions().position(new LatLng(39.953999, -75.187738)).title("Straton Hall"));
            mMap.addMarker(new MarkerOptions().position(new LatLng(39.954744, -75.188586)).title("Korman Center"));
            mMap.addMarker(new MarkerOptions().position(new LatLng(39.955058, -75.186534)).title("Bossone Research Center"));
            mMap.addMarker(new MarkerOptions().position(new LatLng(39.953627, -75.188277)).title("Creese Student Center"));
            mMap.addMarker(new MarkerOptions().position(new LatLng(39.95364, -75.188851)).title("Mandell Theater"));
            mMap.addMarker(new MarkerOptions().position(new LatLng(39.956872, -75.189479)).title("Rush Building"));
            mMap.addMarker(new MarkerOptions().position(new LatLng(39.954524, -75.187698)).title("Disque Hall"));
            mMap.addMarker(new MarkerOptions().position(new LatLng(39.955922, -75.189093)).title("Nesbitt Hall"));
            mMap.addMarker(new MarkerOptions().position(new LatLng(39.956016, -75.189908)).title("Daskalakis Athletic Center"));
            mMap.addMarker(new MarkerOptions().position(new LatLng(39.955194, -75.189908)).title("Hagerty Library"));
        }

    }
}